FROM rocm/tensorflow

#ENV PATH="/home/rocm-user/.local/bin/:$PATH"
#RUN echo $PATH

#RUN pip install jupyter keras tensorflow_datasets tensorflow-text tensorflow

RUN pip install jupyter

#RUN sudo apt-get install libsdl2-dev libsdl2-image-dev libsdl2-mixer-dev libsdl2-ttf-dev libfreetype6-dev libportmidi-dev libjpeg-dev build-essential python3-setuptools python3-dev python3-numpy python3-pip python3-virtualenv cython3 python3-full -y
#RUN git clone https://github.com/pygame/pygame.git
#RUN apt-get update && apt-get install -y lsb-release && apt-get clean all

CMD ["jupyter", "notebook", "--port=8888", "--no-browser", "--ip=0.0.0.0", "--allow-root"]